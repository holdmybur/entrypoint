FROM python:3.7

ADD ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ADD ./entrypoint /entrypoint

WORKDIR /

CMD ["python", "-u", "-m", "entrypoint"]
