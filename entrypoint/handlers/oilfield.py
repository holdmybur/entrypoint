import base64

from aiohttp import web
from ..app import controller

router = web.RouteTableDef()


@router.post('')
async def create_oilfield(request: web.Request, **kwargs) -> web.json_response:
    body = await request.json()
    if not isinstance(body, dict) or not body.get('xlsx'):
        return web.json_response(
            status=400,
            data={'error': 'Incorrect body'}
        )
    xlsx = base64.b64decode(body['xlsx'])

    ok, resp = controller.create_oilfield(xlsx)
    if ok:
        return web.json_response(
            status=201,
            data=resp.to_json_ready(),
        )
    else:
        return web.json_response(
            status=400,
            data={'error': resp}
        )


@router.post('/{oilfield_id}/predict')
async def create_predict(request: web.Request, **kwargs) -> web.json_response:
    oilfield_id = request.match_info['oilfield_id']
    body = await request.json()
    if not isinstance(body, dict) or not body.get('xlsx'):
        return web.json_response(
            status=400,
            data={'error': 'Incorrect body'}
        )
    xlsx = base64.b64decode(body['xlsx'])

    ok, resp = controller.create_predict(oilfield_id, xlsx)
    if ok:
        return web.json_response(
            status=201,
            data=resp.to_json_ready(),
        )
    else:
        return web.json_response(
            status=400,
            data={'error': resp}
        )


@router.get('')
@router.get('/{oilfield_id}')
@router.get('/{oilfield_id}/predict/{predict_id}')
async def get_oilfields_and_predicts(request: web.Request, **kwargs) -> web.json_response:
    oilfield_id = request.match_info.get('oilfield_id')
    if not oilfield_id:
        return web.json_response(
            data=[
                of.to_json_ready()
                for of in controller.values()
            ],
        )

    json_ready_kwargs = {
        'wells': bool(request.rel_url.query.get('wells', False)),
        'wells_date': request.rel_url.query.get('wells_date'),
        'wells_unique_ids': bool(request.rel_url.query.get('wells_unique_ids', False)),
        'wells_by_ids': [
            int(_id)
            if _id.isdecimal()
            else
            _id
            for _id in request.rel_url.query.get('wells_by_ids', '').split(',')
            if _id.strip(' ')
        ],
        'xlsx': bool(request.rel_url.query.get('xlsx', False)),
    }


    oilfield = controller.get(oilfield_id)
    if not oilfield:
        return web.json_response(
            status=404,
            data={'error': 'OilField not found'},
        )

    predict_id = request.match_info.get('predict_id')
    if not predict_id:
        return web.json_response(
            data=oilfield.to_json_ready(
                **json_ready_kwargs
            )
        )
    predict = oilfield.predicts.get(predict_id)
    if not predict:
        return web.json_response(
            status=404,
            data={'error': 'Predict not found'},
        )
    return web.json_response(
        data=predict.to_json_ready(
            **json_ready_kwargs
        )
    )
