import datetime

import pandas as pd
import numpy as np

from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import StandardScaler

from ..oilfield import F, State

class Model:
    FEATURES = [
        "Закачка, м3",
        "Обводненность (вес), %",
        "Диаметр НКТ",
        'Глубина верхних дыр перфорации',
        'Удлинение',
        'Производительность ЭЦН',
        'Напор',
        'Глубина спуска',
        'Буферное давление',
        'Давление в линии',
        'Пластовое давление',
        'Динамическая высота',
        'Забойное давление',
        F.WaterInArea,
    ]

    USEFUL_FEATURES = [
        "Скважина",
        "X",
        "Y"
    ]

    LABELS = ['Нефть, т', 'Вода, т', 'Жидкость, т']

    def __init__(self, scaler=None, models=None):
        self.scaler = scaler or StandardScaler()
        self.models = models or [RandomForestRegressor(n_estimators=200),
                                 RandomForestRegressor(n_estimators=200),
                                 RandomForestRegressor(n_estimators=200)]

    @staticmethod
    def _filter_datetime(x):
        if isinstance(x, datetime.datetime):
            return 0
        return float(x)

    @staticmethod
    def _clear_garbage(df):
        df = df.copy()
        df.fillna(0, inplace=True)
        df = df[df["Время работы, ч"] != 0]
        return df

    def process_features(self, df):
        df = self._clear_garbage(df)

        features = df.loc[:, self.FEATURES]
        #fix dates
        for f in self.FEATURES:
            filtered = features[f].apply(lambda x: self._filter_datetime(x))
            features.loc[:, [f]] = filtered

        self.scaler.fit(features)
        features = self.scaler.transform(features)
        return features

    def process_targets(self, df):
        df = self._clear_garbage(df)

        time = df["Время работы, ч"]
        labels = np.log1p(df[self.LABELS] / time.values.reshape(-1, 1))  # log to get ~normal distribution
        return labels

    def fit(self, df, state: State = None):
        features = self.process_features(df)
        targets = self.process_targets(df)

        state_step = 100 / len(self.models)

        for m, l in zip(self.models, self.LABELS):
            m.fit(features, targets.loc[:, l])
            if state:
                state.inc_progress(state_step)

    def predict(self, df, state: State = None):
        """CAUTION! RUN ONLY AFTER self.predict(...) (for scaler fit)"""
        df = self._clear_garbage(df)

        state_step = 100 / len(self.models)

        features = df.loc[:, self.FEATURES]
        #fix dates
        for f in self.FEATURES:
            filtered = features[f].apply(lambda x: self._filter_datetime(x))
            features.loc[:, [f]] = filtered

        features = self.scaler.transform(features)
        time = df["Время работы, ч"]

        for m, l in zip(self.models, self.LABELS):
            raw_preds = m.predict(features)
            preds = np.expm1(raw_preds) * time
            df[l] = preds
            if state:
                state.inc_progress(state_step)
        return df
