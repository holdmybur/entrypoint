import sys
from dataclasses import dataclass

@dataclass
class State:
    State: str
    Description: str
    Progress: float = .0
    __last_log_progress: float = 0

    def set_progress(self, progress: float):
        self.Progress = progress
        if (self.Progress - self.__last_log_progress) > 10:
            self.__last_log_progress = self.Progress
            print(
                f'State[{self.State}] in {self.Progress}%',
                file=sys.stderr,
            )

    def inc_progress(self, delta: float):
        self.set_progress(self.Progress + delta)

    def to_json_ready(self) -> dict:
        return {
            'state': self.State,
            'description': self.Description,
            'progress': self.Progress,
        }
