from typing import *
import pandas as pd

from .fields import F


class DF:
    @staticmethod
    def filter_by_type(df: pd.DataFrame, t: Any(F.Types.Oil, F.Types.Water)) -> pd.DataFrame:
        return df[df[F.Type] == t]

