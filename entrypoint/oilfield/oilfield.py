import io
import base64
import math
import json
import uuid
from typing import *
import datetime
import pandas as pd
from scipy.spatial.distance import squareform, pdist

from .fields import F
from .state import State

from ..ml import Model


class OilField:
    def __init__(self, data_id: str = None, **kwargs):
        self.created_at = datetime.datetime.utcnow()
        self.completed_at = None
        self.ID = data_id if data_id else str(uuid.uuid4())
        self.parent_oilfield_id = None
        self.dists = {}
        self.opts = kwargs
        self.predicts = {}
        self.df = None
        self.cords = None
        self.model = None
        self.state = State(
            State='initialized',
            Description='OilField Pipeline Initialized',
            Progress=100.,
        )

    def init_data_and_fit(self, data: Union[str, bytes, io.BytesIO, pd.ExcelFile]):
        self.init_data(data)
        self.fit_model()

    def init_data(self, data: Union[str, bytes, io.BytesIO, pd.ExcelFile]):
        self.state = State(
            State='init_data',
            Description='Initializing data',
        )

        xlsx = self._xlsx2df(data)
        self.cords = xlsx.parse(sheet_name=1)
        self.dists = pd.DataFrame(
            squareform(pdist(self.cords.iloc[:, 1:])),
            columns=self.cords[F.WellNum].unique(),
            index=self.cords[F.WellNum].unique(),
        )
        self.df = xlsx.parse(sheet_name=0).join(
            self.cords.set_index(F.WellNum),
            on=F.ID,
        )
        self.state.set_progress(100)
        self.df = self.pre_heuristics(self.df)

    @staticmethod
    def _xlsx2df(data: Union[str, bytes, io.BytesIO, pd.ExcelFile]) -> pd.ExcelFile:
        if isinstance(data, str):
            data = base64.b64decode(data)
        if isinstance(data, bytes):
            data = io.BytesIO(data)
        if isinstance(data, io.BytesIO):
            data = pd.ExcelFile(data, engine='xlrd')
        if isinstance(data, pd.ExcelFile):
            return data

    def completed_state(self):
        self.state = State(
            State='completed',
            Description='Processing completed',
            Progress=100.,
        )
        self.completed_at = datetime.datetime.utcnow()

    def dist(self, id1: Union[int, str], id2: Union[int, str]) -> float:
        return self.dists.get(id1, {}).get(id2, math.inf)

    def pre_heuristics(self, df: pd.DataFrame) -> pd.DataFrame:
        self.state = State(
            State='pre_heuristics',
            Description='Calculation of additional features'
        )
        progress_step = 100 / len(df[df[F.Type] == F.Types.Oil])
        # Water in area
        df[F.WaterInArea] = 0
        for i, well in df[df[F.Type] == F.Types.Oil].iterrows():
            for _, water_well in df[
                (df[F.Type] == F.Types.Water)
                &
                (df[F.Date] == well[F.Date])
            ].iterrows():
                dist = self.dist(well[F.ID], water_well[F.ID])
                if dist <= self.opts.get('water_area_dist', 5000):
                    df[F.WaterInArea][i] += water_well[F.Injection] / dist
            self.state.inc_progress(progress_step)
        return df

    def fit_model(self) -> bool:
        self.state = State(
            State='fit_model',
            Description='Model training'
        )
        self.model = Model()
        self.model.fit(
            self.df[self.df[F.Type] == F.Types.Oil],
            self.state,
        )
        self.completed_state()
        return True

    def predict(self, predict, data: Union[str, bytes, io.BytesIO, pd.ExcelFile]):
        self.predicts[predict.ID] = predict
        predict.parent_oilfield_id = self.ID
        predict.init_data(data)
        predict.state = State(
            State='predict',
            Description='Predicting data',
        )
        predict.df = self.model.predict(
            predict.df, predict.state
        )
        predict.completed_state()
        self.df.append(predict.df)

    def to_json_ready(self,
                      wells: bool = False,
                      wells_date: str = None,
                      wells_unique_ids: bool = False,
                      wells_by_ids: List[Union[int, str]] = None,
                      xlsx: bool = False) -> dict:
        js = {
            'id': self.ID,
            'state': self.state.to_json_ready(),
            'created_at': self.created_at.isoformat(),
            'completed_at': self.completed_at.isoformat() if self.completed_at else None,
        }
        if self.predicts:
            js['predicts'] = [p.to_json_ready() for p in self.predicts.values()]
        if self.parent_oilfield_id:
            js['parent_oilfield_id'] = self.parent_oilfield_id

        if wells and self.df is not None:
            js['wells'] = self.df_serialize(
                wells_date, wells_unique_ids,
                wells_by_ids,
            )
        if xlsx and self.df is not None:
            bt = io.BytesIO()
            with pd.ExcelWriter(bt) as writer:
                self.df.to_excel(writer, sheet_name='Месторождение 1')
                self.cords.to_excel(writer, sheet_name='Координаты')
            js['xlsx'] = base64.b64encode(bt.getvalue()).decode()
        return js

    def df_serialize(self,
                     date: str = None, unique_ids: bool = False,
                     ids: List[Union[str, int]] = None
                     ) -> List[Union[Dict, str, int]]:
        df = self.df
        if unique_ids:
            return [_id for _id, _ in df.groupby(by=F.ID)]

        if ids:
            df = df[df[F.ID].isin(ids)]
        if date:
            if date == 'latest':
                date = df[F.Date].max().strftime('%Y/%m')
            df = df[df[F.Date] == pd.datetime.strptime(date, '%Y/%m')]

        resp = []
        for g, ws in df.groupby(by=F.Date):
            resp.append({
                'month': g.strftime('%Y/%m'),
                'wells': [],
            })
            for i, w in ws.iterrows():
                resp[-1]['wells'].append({
                    'well_id': w[F.ID],
                    'x': w[F.X],
                    'y': w[F.Y],
                    'type': {
                        F.Types.Oil: 'extracting',
                        F.Types.Water: 'pumping',
                    }.get(w[F.Type], 'pumping') if w[F.State] == F.States.Works else 'inactive',
                })
        return resp



