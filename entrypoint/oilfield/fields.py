from typing import *


class F:
    BufferP = 'Буферное давление'
    WaterW = 'Вода, т'
    WorkTime = 'Время работы, ч'
    Date = 'Дата'
    Pressure = 'Напор'
    OilV = 'Нефть, м3'
    OilW = 'Нефть, т'
    LiquidV = 'Жидкость, м3'
    LiquidW = 'Жидкость, т'
    WaterCutPercent = 'Обводненность (вес), %'
    ID = 'Скважина'
    Frequency = 'Частота'
    Injection = 'Закачка, м3'
    Type = 'Характер работы'
    WaterInArea = 'Нагнетаемая вода в области'
    WellNum = '№ скважины'
    X = 'Координата X'
    Y = 'Координата Y'
    State = 'Состояние'

    # Values for fields
    class Types:
        Oil = 'НЕФ'
        Water = 'НАГ'

    class States:
        Works = 'РАБ.'

    @classmethod
    def to_dict(cls) -> Dict[str, str]:
        return {
            k: cls.__dict__[k]
            for k in cls.__annotations__.keys()
        }


