from .handlers.oilfield import router as oilfield_router

from aiohttp import web

class Server:
    app = web.Application(
        client_max_size=20 * 1024 ** 2
    )

    def __init__(self):
        _oilfield_app = web.Application()
        _oilfield_app.add_routes(oilfield_router)
        self.app.add_subapp(
            '/oilfield',
            _oilfield_app,
        )

    def run(self):
        web.run_app(
            app=self.app,
            host='0.0.0.0',
            port=8080,
        )
