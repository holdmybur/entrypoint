from typing import *
import threading
from hashlib import sha256
from ..oilfield import OilField


class Controller(dict):

    def __init__(self):
        super(Controller, self).__init__()
        self.threads = {}

    def get(self, of_id: str) -> OilField:
        return super(Controller, self).get(of_id)

    def values(self) -> ValuesView[OilField]:
        return super(Controller, self).values()

    def _run_thread(self, of_id, target, args):
        self.threads[of_id] = threading.Thread(
            target=target,
            args=args,
        )
        self.threads[of_id].start()

    def create_oilfield(self, data: bytes) -> Tuple[bool, Union[OilField, str]]:
        data_id = sha256(data).digest().hex()
        of = self.get(data_id)
        if of:
            return True, of
        of = OilField(data_id=data_id)
        self[of.ID] = of
        self._run_thread(
            of.ID,
            of.init_data_and_fit,
            args=(data,),
        )
        return True, of

    def create_predict(self, of_id: str, data: bytes) -> Tuple[bool, Union[OilField, str]]:
        of = self.get(of_id)
        if not of:
            return False, 'Does not exists'

        if not of.model:
            return False, 'Model does not predicted'

        data_id = sha256(data).degist().hex()
        predict = of.predicts.get(data_id)
        if predict:
            return True, predict
        predict = OilField(data_id=data_id)
        self._run_thread(
            predict.ID,
            of.predict,
            args=(predict,data),
        )
        return True, predict
